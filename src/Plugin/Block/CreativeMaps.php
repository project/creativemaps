<?php

/**
 * @file
 * Contains \Drupal\creativemaps\Plugin\Block\CreativeMaps.
 */

namespace Drupal\creativemaps\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\taxonomy\Entity\Term;



/**
 * Provides a 'entitlement link' block.
 *
 * @Block(
 *   id = "creativemaps_block",
 *   admin_label = @Translation("Creative Maps"),
 *   category = @Translation("Custom creative map block")
 * )
 */
class CreativeMaps extends BlockBase {
    
      /**
   * {@inheritdoc}
   */
  public function build() {
     $statedata="";
$query = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('type', 'state');
    
$nids = $query->execute();
    $statesdata = $this->getStatesData($nids);
     return array(
          '#theme' => 'creativemaps',
          '#data' => $statesdata,
          '#attached' => array(
            'library' => array(
             'creativemaps/creative-maps',
             ),
           ),
          '#cache' => array(
          'max-age' => 0
          ),
         
        );
  }
  
  
  
  public static function getStatesData($nids){
    $termdata=[];     
      $indian_all_states  = array (
 'IN-AP' => 'Andhra Pradesh',
 'IN-AR' => 'Arunachal Pradesh',
 'IN-AS' => 'Assam',
 'IN-BR' => 'Bihar',
 'IN-CH' => 'Chandigarh',       
 'IN-CT' => 'Chhattisgarh',
 'IN-GA' => 'Goa',
 'IN-GJ' => 'Gujarat',
 'IN-HR' => 'Haryana',
 'IN-HP' => 'Himachal Pradesh',
 'IN-JK' => 'Jammu & Kashmir',
 'IN-JH' => 'Jharkhand',
 'IN-KA' => 'Karnataka',
 'IN-KL' => 'Kerala',
 'IN-MP' => 'Madhya Pradesh',
 'IN-MH' => 'Maharashtra',
 'IN-MN' => 'Manipur',
 'IN-ML' => 'Meghalaya',
 'IN-MZ' => 'Mizoram',
 'IN-NL' => 'Nagaland',
 'IN-OR' => 'Odisha',
 'IN-PB' => 'Punjab',
 'IN-RJ' => 'Rajasthan',
 'IN-SK' => 'Sikkim',
 'IN-TN' => 'Tamil Nadu',
 'IN-TS' => 'Telanagana',
 'IN-TR' => 'Tripura',
 'IN-UP' => 'Uttar Pradesh',
 'IN-UK' => 'Uttarakhand',
 'IN-WB' => 'West Bengal',
 'IN-AN' => 'Andaman & Nicobar',
 'IN-CH' => 'Chandigarh',
 'IN-DN' => 'Dadra and Nagar Haveli',
 'IN-DD' => 'Daman & Diu',
 'IN-DL' => 'Delhi',
 'IN-LD' => 'Lakshadweep',
 'IN-PY' => 'Puducherry',
);
    $i=0;
    foreach($nids as $nid){
        $nodedata=node_load($nid);
        $options = ['absolute' => TRUE];
        $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $nid], $options);
        $url = $url->toString();
        $statedata[$i]['name']= $nodedata->getTitle();
        $statedata[$i]['url'] = $url;
        $statedata[$i]['code']= array_search($nodedata->getTitle(),$indian_all_states); 
        $i++;
    }
   
        return $statedata;
    }
    
/*  public static function getIndianstateCodes($statename){
  $indian_all_states  = array (
 'AP' => 'Andhra Pradesh',
 'AR' => 'Arunachal Pradesh',
 'AS' => 'Assam',
 'BR' => 'Bihar',
 'CT' => 'Chhattisgarh',
 'GA' => 'Goa',
 'GJ' => 'Gujarat',
 'HR' => 'Haryana',
 'HP' => 'Himachal Pradesh',
 'JK' => 'Jammu & Kashmir',
 'JH' => 'Jharkhand',
 'KA' => 'Karnataka',
 'KL' => 'Kerala',
 'MP' => 'Madhya Pradesh',
 'MH' => 'Maharashtra',
 'MN' => 'Manipur',
 'ML' => 'Meghalaya',
 'MZ' => 'Mizoram',
 'NL' => 'Nagaland',
 'OR' => 'Odisha',
 'PB' => 'Punjab',
 'RJ' => 'Rajasthan',
 'SK' => 'Sikkim',
 'TN' => 'Tamil Nadu',
 'TR' => 'Tripura',
 'UK' => 'Uttarakhand',
 'UP' => 'Uttar Pradesh',
 'WB' => 'West Bengal',
 'AN' => 'Andaman & Nicobar',
 'CH' => 'Chandigarh',
 'DN' => 'Dadra and Nagar Haveli',
 'DD' => 'Daman & Diu',
 'DL' => 'Delhi',
 'LD' => 'Lakshadweep',
 'PY' => 'Puducherry',
);
  $statecode= array_search($statename,$indian_all_states); 
return $statecode; 
}*/
    
    
}
