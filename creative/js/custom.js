
var statesdata =JSON.parse(drupalSettings.statedata);

console.log(statesdata);
var currentItem;

var map = AmCharts.makeChart("mapdiv", {
  "type": "map",
  "theme": "dark",
  addClassNames: true,
  fontSize:15,
  //developerMode:true,
  tapToActivate:false,
  dragMap:false,
  descriptionWindowY:300,
  "balloon": {
     "textAlign": "left",
     "adjustBorderColor": false,
     borderColor:'transparent',
    "color": "#db3f33",
    "cornerRadius": 5,
    "fontSize":15,
    "maxWidth":300,
    "fillColor": "#eee",
     "borderThickness": 1,
    "borderAlpha":2,
    "fillAlpha":.7,
    "horizontalPadding":10,
    "verticalPadding": 10,
    "shadowAlpha": 0,
    shadowColor:"#fff"
    },
   // "smallMap": {
	//	"backgroundAlpha": 0.5
	//},
        
  "dataProvider": {
    "map": "indiaLow",
    "areas": statesdata
 
   
  },
  "zoomControl": {
		"zoomControlEnabled": false,
                "homeButtonEnabled": false,
                "draggerAlpha": 0
	},
  "areasSettings": {
              autoZoom: false,
              selectedColor: "#e00065",
              color:"#f4cb00",
              borderColor:"#e00065",
              ballonText:"[[title]]",
              selectable: true,
              alpha: 1.5
  },
  "ammap":{
     // autoTransform:true,
      dragMap:false
  },
  "images": [
      {
        "type": "circle",
        "top": 100,
        "left": 100
      }],
  "listeners": [{
    "event": "clickMapObject",
    "method": function(event) {
      console.log(event.mapObject.id);
      window.location = document.getElementById(event.mapObject.id).getAttribute('href');
    }},{
    "event":"rollOverMapObject",
     "method": function(event) {
         var id = event.mapObject.id;

        var lights = document.getElementsByClassName("selected");
       while (lights.length)
    lights[0].classList.remove("selected");
        var list = document.getElementById("states_list").getElementsByTagName('a');
       //console.log(list); 
  for (var key in list) {
    if (list[key].id === event.mapObject.id) {
      //  alert(list[key].id );
      list[key].className = 'selected';
                //event.mapObject.colorReal = "#e00065";
      currentItem = list[key];
    }
  }
     }
     
  },{
    "event":"rollOutMapObject",
     "method": function(event) {
         event.mapObject.colorReal = "#f4cb00";
     }
     }]
});
                        
                        
 function selectArea(state) {
     
     var lights = document.getElementsByClassName("selected");
while (lights.length)
    lights[0].classList.remove("selected");
    var id = state;
    if ( '' === id ) {
        map.rollOverMapObject(map.dataProvider);
    }
    else {
       var area = map.getObjectById(id);
  area.color = '#' + "e00065";
        map.rollOverMapObject(map.getObjectById(id));
        //map.rollOutMapObject( map.getObjectById(id));
    }
}

